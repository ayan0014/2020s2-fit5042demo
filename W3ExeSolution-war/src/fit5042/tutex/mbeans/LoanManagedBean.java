/*
 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.mbeans;

import java.io.Serializable;


import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.enterprise.context.SessionScoped;

import fit5042.tutex.repository.entities.Loan;
import fit5042.tutex.calculator.MonthlyPaymentCalculator;
//import fit5042.tutex.calculator.MonthlyPaymentCalculatorSessionBean;

/**
 *
 * @author: originally created by Eddie. The following code has been changed in
 *          order for students to practice.
 */
@ManagedBean(name = "loanManagedBean", eager = true)
@SessionScoped
public class LoanManagedBean implements Serializable {
	
	@EJB
	private MonthlyPaymentCalculator mpc;

	private Loan loan;

	public LoanManagedBean() {
		this.loan = new Loan();

	}

	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	public String calculate(){
		//Andong Yang
		//put the monthlyPayment value back from EJB to interface, and to the loan class with set method.
		double monthlyPayment = mpc.calculate(loan.getPrinciple(),loan.getNumberOfYears(),loan.getInterestRate());
        loan.setMonthlyPayment(monthlyPayment);
        
		return "index";
	}
}