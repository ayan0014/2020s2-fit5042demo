package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;

import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial
 * instructions. This is the main driver class. This class contains the main
 * method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Andong Yang
 * @StudentID 30054605
 */
public class RealEstateAgency {
	private String name;
	private final PropertyRepository propertyRepository;
	Scanner sc = new Scanner(System.in);

	public RealEstateAgency(String name) throws Exception {
		this.name = name;
		this.propertyRepository = PropertyRepositoryFactory.getInstance();
	}

	// this method is for initializing the property objects
	// complete this method
	public void createProperty() {

		// System.out.println("Please input at least 5 properties");
		Property p1 = new Property(1, "83,normanby rd", 2, 118, 1230000);
		Property p2 = new Property(2, "84,normanby rd", 1, 93, 1000000);
		Property p3 = new Property(3, "85,normanby rd", 5, 250, 4230000);
		Property p4 = new Property(4, "86,normanby rd", 2, 120, 1360000);
		Property p5 = new Property(5, "87,normanby rd", 2, 100, 1130000);

		try {
			propertyRepository.addProperty(p1);
			propertyRepository.addProperty(p2);
			propertyRepository.addProperty(p3);
			propertyRepository.addProperty(p4);
			propertyRepository.addProperty(p5);
			System.out.println("Property has been added to the List");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// this method is for displaying all the properties
	// complete this method
	public void displayProperties() {
		List<Property> p;
		try {
			p = propertyRepository.getAllProperties();
			for (Property property : p) {
				System.out.println(property.toString());

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// this method is for searching the property by ID
	// complete this method
	public void searchPropertyById() {
		System.out.println("Enter the ID of the property you want to search:");
		int id = sc.nextInt();
		try {
			Property propertyById = propertyRepository.searchPropertyById(id);
			System.out.println(propertyById.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void run() {
		createProperty();
		System.out.println("********************************************************************************");
		displayProperties();
		System.out.println("********************************************************************************");
		searchPropertyById();
	}

	public static void main(String[] args) {
		try {
			new RealEstateAgency("FIT5042 Real Estate Agency").run();
		} catch (Exception ex) {
			System.out.println("Application fail to run: " + ex.getMessage());
		}
	}
}
