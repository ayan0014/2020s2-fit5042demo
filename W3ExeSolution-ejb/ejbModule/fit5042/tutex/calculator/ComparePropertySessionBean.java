package fit5042.tutex.calculator;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;
//import javax.ejb.Remote;
import javax.ejb.Stateful;

@Stateful
public class ComparePropertySessionBean implements CompareProperty {
	List<Property> p;

	public ComparePropertySessionBean() {
		p = new ArrayList<Property>();
	}

	@Override
	public void addProperty(Property property) {
		// TODO Auto-generated method stub
		p.add(property);

	}

	@Override
	public void removeProperty(Property property) {
		// TODO Auto-generated method stub
		for (Property ps : p) {
			if (ps.getPropertyId() == property.getPropertyId()) {
				p.remove(property);
			}
		}
	}

	@Override
	public int getBestPerRoom() {
		int bestPerRoom = 0;
		// TODO Auto-generated method stub
		double min = 10000000.00;
		for (int i = 0; i <= p.size() - 1; i++) {
			if ((p.get(i).getPrice() / p.get(i).getNumberOfBedrooms()) <= min) {
				min = p.get(i).getPrice() / p.get(i).getNumberOfBedrooms();
			}
		}

		// int bestPerRoom;
		for (Property ps : p) {
			for(int j = 0; j <= p.size() - 1; j++ ) {
				if (p.get(j).getPrice() / p.get(j).getNumberOfBedrooms() == min) {
					bestPerRoom = p.get(j).getPropertyId();
					//return bestPerRoom;
				}
			}
		}
		return bestPerRoom;
	}

}
